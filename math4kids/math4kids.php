<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Title</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='css/main.css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Bevan&display=swap" rel="stylesheet">
    <script src='main.js'></script>
</head>
<body>
<div class=container>

        <br><br><br>

    <div class="x-100">
        <span class=y-100>1</span>
    </div>
    <div class="x-200">
        <span class=y-100>+</span>
    </div>
    <div class="x-300">
        <span class=y-100>3</span>
    </div>
    <div class="x-200">
        <span class=y-100>=</span>
    </div>
    <div class="x-400">
        <span class=y-100>?</span>
    </div>

</div>

<
</body>
</html>